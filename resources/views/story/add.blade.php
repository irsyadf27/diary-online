@extends('../app')

@section('title')
<title>Add New Story | Diary Online</title>
@endsection

@section('css')
    <link href="{{ asset('/plugins/bootstrap-wysihtml5/src/bootstrap3-wysihtml5.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('sidebar')
<li class="header">Menu</li>
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="{{ url('timeline') }}"><i class="fa fa-clock-o"></i> Timeline</a></li>
<li><a href="{{ url('media') }}"><i class="fa fa-picture-o"></i> Media</a></li>
@endsection

@section('header')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Add New Story
            <small>Diary Online</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Add New Story</li>
          </ol>
        </section>
@endsection

@section('content')

          <div class='row'>
            <div class='col-md-12'>
            @if (count($errors) > 0)
            <div class="alert alert-danger alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h4><i class="icon fa fa-ban"></i> Alert!</h4>
              <ul>
                @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
                @endforeach
              </ul>
            </div>
            @endif

            @if (Session::get('sukses'))
            <div class="alert alert-success alert-dismissable">
                <i class="fa fa-check"></i>
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <b>Success!</b> {{ Session::get('sukses') }}
            </div>
            @endif

            <form role="form" method="POST" action="{{ url('/story/new') }}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="rand" value="{{ str_random(10) }}">
             <!-- Input addon -->
              <div class="box">
                  <div class="box-header">
                      <h3 class="box-title"><i class="fa fa-plus-square-o"></i> Add Story</h3>
                  </div>
                  <div class="box-body">
                      <div class="input-group input-group-md">
                          <span class="input-group-addon">Title</span>
                          <input type="text" name="title" class="form-control" placeholder="Title">
                      </div>
                  </div><!-- /.box-body -->
              </div><!-- /.box -->
              <div class='box'>
                <div class='box-header'>
                  <h3 class='box-title'>Bootstrap WYSIHTML5 <small>Simple and fast</small></h3>
                  <!-- tools box -->
                  <!-- <div class="pull-right box-tools">
                    <button class="btn btn-default btn-sm" data-widget='collapse' data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
                    <button class="btn btn-default btn-sm" data-widget='remove' data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>
                  </div> -->
                  <!-- /. tools -->
                </div><!-- /.box-header -->
                <div class='box-body'>
                    <textarea class="textarea" name="story" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                </div>
              </div>
              <div class="box">
                <div class="box-body">
                  <div class="btn-group">
                    <button type="submit" name="save_story" value="save" class="btn btn-info"><i class='fa fa-align-left'></i> Save as Story</button>
                    <button type="submit" name="save_draft" value="save" class="btn btn-info"><i class='fa fa-align-center'></i> Save as Draft</button>
                  </div>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
              </form>
            </div><!-- /.col-->
          </div><!-- ./row -->
@endsection

@section('js')
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('/plugins/bootstrap-wysihtml5/dist/wysihtml5x-toolbar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/plugins/bootstrap-wysihtml5/src/bootstrap3-wysihtml5.js') }}" type="text/javascript"></script>
<script type="text/javascript">
  $(function () {
    // Replace the <textarea id="editor1"> with a CKEditor
    // instance, using default configuration.
    //bootstrap WYSIHTML5 - text editor
    $(".textarea").wysihtml5({
      stylesheets: ["{{ asset('/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5-editor.css') }}"],
      "font-styles":  true, //Font styling, e.g. h1, h2, etc
      "color":        true, //Button to change color of font
      "emphasis":     true, //Italics, bold, etc
      "textAlign":    true, //Text align (left, right, center, justify)
      "lists":        true, //(Un)ordered lists, e.g. Bullets, Numbers
      "blockquote":   true, //Button to insert quote
      "link":         true, //Button to insert a link
      "table":        true, //Button to insert a table
      "image":        true, //Button to insert an image
      "video":        true, //Button to insert video (YouTube, Vimeo, Metacafe and DailyMotion supported)
      "html":         true //Button which allows you to edit the generated HTML
      });
  var panjang_cerita = 0;
  function strip(html) {
      // Stripping the legal HTML tags
      html = html.replace(/<[^>]*>/g, '');

      // Escaping the remaining characters
      var div = document.createElement('div');
      div.textContent = html;
      return div.innerHTML;
  }
  setInterval(function () {
    story = strip($("[name='story']").val());
    console.log('cerita = ' + story.length + ' | panjang cerita = ' + panjang_cerita);
    if(story.length != panjang_cerita && story.length > 20){
      console.log('save cerita = ' + story.length + ' | panjang cerita = ' + panjang_cerita);
      $.post("{{ url('ajax/save_draft') }}", $("form").serialize());
    }
    panjang_cerita = story.length
  }, 180000);
});
</script>
@endsection