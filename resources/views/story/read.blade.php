@extends('../app')

@section('title')
<title>{{ $data['title_full'] }} | Diary Online</title>
@endsection

@section('sidebar')
<li class="header">Menu</li>
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="{{ url('timeline') }}"><i class="fa fa-clock-o"></i> Timeline</a></li>
<li><a href="{{ url('media') }}"><i class="fa fa-picture-o"></i> Media</a></li>
@endsection

@section('header')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Read Story
            <small>Diary Online</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Read Story</li>
          </ol>
        </section>
@endsection

@section('content')

          <div class='row'>
            <div class='col-md-12'>
              <div class="box box-solid">
                <div class="box-header with-border">
                  <i class="fa fa-text-width"></i>
                  <h3 class="box-title">{{ $data['title_full'] }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  {!! $data['body_full'] !!}
                  <h6 class="text-muted text-right"><i class="fa fa-pencil"> {{ $data['created_at'] }} - By: {{ Auth::user()->name }}</i></h6>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- ./col -->
          </div><!-- ./row -->
@endsection
