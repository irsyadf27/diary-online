@extends('../app')

@section('title')
<title>List Story | Diary Online</title>
@endsection

@section('css')
    <!-- DATA TABLES -->
    <link href="{{ asset('/plugins/datatables/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('sidebar')
<li class="header">Menu</li>
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="{{ url('timeline') }}"><i class="fa fa-clock-o"></i> Timeline</a></li>
<li><a href="{{ url('media') }}"><i class="fa fa-picture-o"></i> Media</a></li>
@endsection

@section('header')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            List Story
            <small>Diary Online</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">List Story</li>
          </ol>
        </section>
@endsection

@section('content')

          <div class='row'>
            <div class='col-md-12'>
              <div id="alertFailed" style="display:none;">
                <div class="alert alert-danger alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                  <p id="msgFailed"></p>
                </div>
              </div>
              <div id="alertSuccess" style="display:none;">
                <div class="alert alert-success alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4><i class="icon fa fa-check"></i> Alert!</h4>
                  <p id="msgSuccess"></p>
                </div>
              </div>
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Data Table With Full Features</h3>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th>Title</th>
                        <th>Type</th>
                        <th>Create</th>
                        <th>Update</th>
                        <th>Delete</th>
                        <th></th>
                      </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $post)
                      <tr>
                        <td>{{ str_limit($post['title'], 50) }}</td>
                        <td>{{ ucfirst($post['save']) }}</td>
                        <td>{{ $post['created_at'] }}</td>
                        <td>{{ $post['updated_at'] }}</td>
                        <td>{{ $post['deleted_at'] }}</td>
                        <td class="text-center">
                          <div class="btn-group">
                            <a class="btn btn-flat bg-olive confirm-restore" data-id="{{ $post['id'] }}" data-title="{{ $post['title'] }}" ><i class="fa fa-refresh"></i></a>
                            <a class="btn btn-flat btn-danger confirm-delete" data-id="{{ $post['id'] }}" data-title="{{ $post['title'] }}" ><i class="fa fa-times"></i></a>
                          </div>
                        </td>
                      </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                      <tr>
                        <th>Title</th>
                        <th>Type</th>
                        <th>Create</th>
                        <th>Update</th>
                        <th>Delete</th>
                        <th></th>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col-->
            <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-danger">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Want to delete?</h4>
                    </div>
                    <div class="modal-body">
                      <p id="msgDelete"></p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                      <button type="button"  id="btnDelete" class="btn btn-outline">Delete</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->
            </div>
            <div class="modal fade" id="modalRestore" tabindex="-1" role="dialog" aria-hidden="true">
              <div class="modal-success">
                <div class="modal-dialog">
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                      <h4 class="modal-title">Want to restore?</h4>
                    </div>
                    <div class="modal-body">
                      <p id="msgRestore"></p>
                    </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                      <button type="button"  id="btnRestore" class="btn btn-outline">Restore</button>
                    </div>
                  </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
              </div><!-- /.modal -->
            </div>
          </div><!-- ./row -->
@endsection

@section('js')
<!-- DATA TABES SCRIPT -->
<script src="{{ asset('/plugins/datatables/jquery.dataTables.js') }}" type="text/javascript"></script>
<script src="{{ asset('/plugins/datatables/dataTables.bootstrap.js') }}" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
  $('#example1').dataTable({
    "aaSorting": [[ 2, "asc" ]],
    "aoColumns":[
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": true},
        {"bSortable": false},
    ]
  });

  $('.confirm-delete').on('click', function(e) {
    e.preventDefault();

    var id = $(this).data('id');
    var title = $(this).data('title');
    $('#msgDelete').text('Are you sure you want to force delete "' + title + '" ?');
    $('#modalDelete').data('title', title);
    $('#modalDelete').data('id', id).modal('show');
  });
  $('#btnDelete').click(function() {
    var id = $('#modalDelete').data('id');
    var title = $('#modalDelete').data('title');
    $.ajax({
      type    :"POST",
      url     :"{{ url('ajax/forcedelete_story') }}",
      dataType:"json",
      data    :{ 'id': id, 'title': title, '_token': '{{ csrf_token() }}' },
      success :function(res) {
        if(res.success == true){
          $('#msgSuccess').text(res.data);
          $('#tl' + id).hide();
          $('#alertSuccess').show();
        }else{
          $('#msgFailed').text(res.data);
          $('#alertFailed').show();
        }
      },
      error: function(e) {
        console.log(e.responseText);
      }
    });
    $('#modalDelete').modal('hide');
  });

  $('.confirm-restore').on('click', function(e) {
    e.preventDefault();

    var id = $(this).data('id');
    var title = $(this).data('title');
    $('#msgRestore').text('Are you sure you want to restore "' + title + '" ?');
    $('#modalRestore').data('title', title);
    $('#modalRestore').data('id', id).modal('show');
  });
  $('#btnRestore').click(function() {
    var id = $('#modalRestore').data('id');
    var title = $('#modalRestore').data('title');
    $.ajax({
      type    :"POST",
      url     :"{{ url('ajax/restore_story') }}",
      dataType:"json",
      data    :{ 'id': id, 'title': title, '_token': '{{ csrf_token() }}' },
      success :function(res) {
        if(res.success == true){
          $('#msgSuccess').text(res.data);
          $('#tl' + id).hide();
          $('#alertSuccess').show();
        }else{
          $('#msgFailed').text(res.data);
          $('#alertFailed').show();
        }
      },
      error: function(e) {
        console.log(e.responseText);
      }
    });
    $('#modalRestore').modal('hide');
  });
});
</script>
@endsection