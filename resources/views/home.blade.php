@extends('app')

@section('title')
<title>Dashboard | Diary Online</title>
@endsection

@section('sidebar')
<li class="header">Menu</li>
<li class="active"><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="{{ url('timeline') }}"><i class="fa fa-clock-o"></i> Timeline</a></li>
<li><a href="{{ url('media') }}"><i class="fa fa-picture-o"></i> Media</a></li>
@endsection

@section('header')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Diary Online</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
@endsection

@section('content')
          <!-- Info boxes -->
          <div class="row">
            <div class="col-md-8">
              <div class="info-box">
                <a href="{{ url('story') }}"><span class="info-box-icon bg-aqua"><i class="ion ion-grid"></i></span></a>
                <div class="info-box-content">
                  <span class="info-box-text">Story</span>
                  <span class="info-box-number">{{ $jml_story }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->

              <div class="info-box">
                <a href="{{ url('story/draft') }}"><span class="info-box-icon bg-red"><i class="fa ion-clipboard"></i></span></a>
                <div class="info-box-content">
                  <span class="info-box-text">Drafts</span>
                  <span class="info-box-number">{{ $jml_draft }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->


            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>


              <div class="info-box">
                <a href="{{ url('story/trash') }}"><span class="info-box-icon bg-red"><i class="ion ion-trash-b"></i></span></a>
                <div class="info-box-content">
                  <span class="info-box-text">Trash</span>
                  <span class="info-box-number">{{ $jml_trash }}</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->

              <div class="info-box">
                <a href="{{ url('/story/addstory') }}"><span class="info-box-icon bg-yellow"><i class="ion ion-plus"></i></span></a>
                <div class="info-box-content">
                  <span class="info-box-text"></span>
                  <span class="info-box-number">Add new story</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->

              <!--<div class="info-box">
                <a href="{{ url('/media/addmedia') }}"><span class="info-box-icon bg-green"><i class="ion ion-plus"></i></span></a>
                <div class="info-box-content">
                  <span class="info-box-text"></span>
                  <span class="info-box-number">Add new media</span>
                </div>
              </div>-->

            </div>
            <div class="col-md-4">
              <div class="card hovercard">
               <img src="{{ asset('dist/img/photo1.png') }}" alt=""/>
               <div class="avatar">
                  <img src="{{ url('account/img/160x160/'.Auth::user()->id) }}" alt="" class="img-circle" />
               </div>
               <div class="info">
                  <div class="title">
                     {{ Auth::user()->name }}
                  </div>
                  <div class="desc">{{ Auth::user()->bio }}</div>
               </div>
               <div class="bottom">
                  <div class="">
                    <p><i class="fa fa-user"></i> {{ Auth::user()->username }}</p>
                    <p><i class="fa fa-envelope"></i> {{ Auth::user()->email }}</p>
                    <p><i class="fa fa-calendar"></i> {{ $tgl_lahir1 }} ({{ $tgl_lahir2 }})</p>
                  </div>
               </div>
            </div>
          </div>
          </div><!-- /.row -->
@endsection
