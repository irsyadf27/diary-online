@extends('app')

@section('title')
<title>Dashboard | Diary Online</title>
@endsection

@section('sidebar')
<li class="header">Menu</li>
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="{{ url('/timeline') }}"><i class="fa fa-clock-o"></i> Timeline</a></li>
<li class="active"><a href="#"><i class="fa fa-clock-o"></i> Media</a></li>
@endsection

@section('header')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small>Diary Online</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>
@endsection

@section('content')
          <!-- Info boxes -->
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <a href="{{ url('timeline') }}"><span class="info-box-icon bg-aqua"><i class="ion ion-grid"></i></span></a>
                <div class="info-box-content">
                  <span class="info-box-text">Story</span>
                  <span class="info-box-number">100</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <a href="#"><span class="info-box-icon bg-red"><i class="fa ion-clipboard"></i></span></a>
                <div class="info-box-content">
                  <span class="info-box-text">Drafts</span>
                  <span class="info-box-number">0</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->

            <!-- fix for small devices only -->
            <div class="clearfix visible-sm-block"></div>

            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <a href="#"><span class="info-box-icon bg-red"><i class="ion ion-trash-b"></i></span></a>
                <div class="info-box-content">
                  <span class="info-box-text">Trash</span>
                  <span class="info-box-number">10</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="info-box">
                <a href="{{ url('addstory') }}"><span class="info-box-icon bg-yellow"><i class="ion ion-plus"></i></span></a>
                <div class="info-box-content">
                  <span class="info-box-text"></span>
                  <span class="info-box-number">Add new story</span>
                </div><!-- /.info-box-content -->
              </div><!-- /.info-box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
@endsection
