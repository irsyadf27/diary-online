@extends('../app')

@section('title')
<title>Media | Diary Online</title>
@endsection

@section('css')
    <link href="{{ asset('/plugins/prettyPhoto/css/prettyPhoto.css') }}" rel="stylesheet" type="text/css" />
<style>
.prev_container{
  overflow: auto;
}
.prev_thumb{
  height: 80px;
  width: 100px;
}
</style>
@endsection

@section('sidebar')
<li class="header">Menu</li>
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="{{ url('timeline') }}"><i class="fa fa-clock-o"></i> Timeline</a></li>
<li class="active"><a href="#"><i class="fa fa-picture-o"></i> Media</a></li>
@endsection

@section('header')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Media
            <small>Diary Online</small>
            <button class="btn btn-sm btn-primary btn-flat" data-toggle="modal" data-target="#modalAdd" title="Add picture" style="color:#fff;"><i class="fa fa-plus"></i> Add picture</button>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Media</li>
          </ol>
        </section>
@endsection

@section('content')
      <div class="row">
        <div id="alertFailed" style="display:none;">
          <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
            <p id="msgFailed"></p>
          </div>
        </div>
        <div id="alertSuccess" style="display:none;">
          <div class="alert alert-success alert-dismissable">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> Alert!</h4>
            <p id="msgSuccess"></p>
          </div>
        </div>
      </div>
      <div class="row" id="image-grid">
        <div class="col-md-12">
        @foreach($data as $media)
          <div class="col-md-3 image-item" id="media{{ $media->id }}">
            <div class="box">
              @if(!empty($media->title))
                <div class="box-header">
                  <p class="box-title">{{ $media->title }}</p>
                </div>
              @endif
              <div class="box-body">
              <a href="{{ url('media/'.$media->md5_time) }}" rel="prettyPhoto" title="<h6 class='text-muted'><i class='fa fa-clock-o'></i> {{ $media->created_at }}</h6>"><img class="img-responsive" style="width:100%;" src="{{ url('media/thumb_'.$media->md5_time) }}"/></a>
              </div><!-- /.box-body -->
              <div class="box-footer">
              <p>{{ $media->desc }}</p>
              <h6 class="text-muted"><i class="fa fa-clock-o"></i> {{ $media->created_at }} <!--<a href='#'>Edit</a>--> - <a href='#' class='confirm-delete' data-id='{{ $media->id }}' data-img='{{ url('media/thumb_'.$media->md5_time) }}'>Delete</a> </h6>
              </div><!-- /.box-body -->
            </div><!-- /.box -->
          </div>
        @endforeach

        </div>
          <div class="modal fade" id="modalAdd" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-default">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add picture</h4>
                  </div>
                  <div class="modal-body">
                    <div id="bukuResult"></div>
                    <div id="loading" class="text-center" style="display:none;"><img src="{{ asset('dist/img/ajax-loader1.gif') }}" title="Loading..."/><br/>Jangan di tutup sebelum loading selesai...</div>
                    <div id="modalForm">
                      <form action="{{ url('ajax/addmedia') }}" method="post" id="addForm">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <!-- text input -->
                        <div class="form-group">
                          <label>Title</label>
                          <input class="form-control" name="title" placeholder="Title ..." type="text">
                        </div>

                        <!-- textarea -->
                        <div class="form-group">
                          <label>Description</label>
                          <textarea class="form-control" name="desc" rows="3" placeholder="Description ..."></textarea>
                        </div>

                        <div class="form-group">
                          <label>Picture</label>
                          <input class="file" id="file1" name="pict" type='file' multiple title="test #1"/>
                        </div>
                        <div class="form-group">
                          <label>Preview</label>
                          <div id="prev_file1"></div>
                        </div>
                    </div>
                    <div class="modal-footer">
                      <button class="btn btn-danger pull-left" data-dismiss="modal">Close</button>
                      <button type="submit" id="btnUpload" class="btn btn-success">Upload</button>
                    </div>
                    </form>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div>
      </div><!-- ./row -->
          <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-danger">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Want to delete?</h4>
                  </div>
                  <div class="modal-body">
                    <p id="msgDelete"></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="button" id="btnDelete" class="btn btn-outline">Delete</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div>
@endsection

@section('js')
<script src="http://malsup.github.com/jquery.form.js"></script> 
<script src="http://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/2.2.0/isotope.pkgd.js"></script>
<script src="{{ asset('/plugins/prettyPhoto/js/jquery.prettyPhoto.js') }}"></script>
<script src="{{ asset('/plugins/jquery.preimage.js') }}"></script>
<script>
  $(function () {
    $('#image-grid').isotope({
      // options
      itemSelector: '.image-item',
      layoutMode: 'masonry',
    });
    $(window).resize();
    $("a[rel^='prettyPhoto']").prettyPhoto({
      theme: 'pp_default',
      allow_resize: true,
      social_tools: '',
    });
    function processJson(data) { 
        if(data.fail) {
            alert(data.msg);
            $('#modalForm').show();
            $('#loading').hide();
            $('#bukuResult').html('<div class="alert alert-danger alert-dismissable"><i class="fa fa-ban"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><b>Failed to upload.</b><br/><span id="msgTambahan"></span></div>');
            $.each(data.msg, function( index, value ) {
                $('#msgTambahan').append(value + '<br />');
            });
        }
        if(data.success) {
            $('#modalForm').show();
            $('#loading').hide();
            $('#bukuResult').html('<div class="alert alert-success alert-dismissable"><i class="fa fa-check"></i></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><b>' + data.msg + '</b><br/><span id="msgTambahan"></span></div>');
            if(data.title != ""){
              var val = $('<div class="col-md-3 image-item"><div class="box"><div class="box-header"><p class="box-title">' + data.title +'</p></div><div class="box-body"><img class="img-responsive" style="width:100%;" src="' + data.url +'"/></div><!-- /.box-body --><div class="box-footer"><p>' + data.desc +'</p><h6 class="text-muted"><i class="fa fa-clock-o"></i> ' + data.created_at +'</h6></div><!-- /.box-body --></div><!-- /.box --></div>');
            } else {
              var val = $('<div class="col-md-3 image-item"><div class="box"><div class="box-body"><img class="img-responsive" style="width:100%;" src="' + data.url +'"/></div><!-- /.box-body --><div class="box-footer"><p>' + data.desc +'</p><h6 class="text-muted"><i class="fa fa-clock-o"></i> ' + data.created_at +'</h6></div><!-- /.box-body --></div><!-- /.box --></div>');
            }
            $('#image-grid').prepend(val).isotope( 'reload' );
            }
    }
    function bef(){
        $('#modalForm').hide();
        $('#loading').show();
    }
    $('#addForm').ajaxForm({
        beforeSubmit: bef,
        dataType: 'json',
        success: processJson
    });
    $('.file').preimage();
    $('.confirm-delete').on('click', function(e) {
      e.preventDefault();

      var id = $(this).data('id');
      var img = $(this).data('img');
      $('#msgDelete').html('Are you sure you want to delete ?<br/><img src="' + img + '">');
      $('#modalDelete').data('id', id).modal('show');
    });
    $('#btnDelete').click(function() {
      var id = $('#modalDelete').data('id');
      $.ajax({
        type    :"POST",
        url     :"{{ url('ajax/delete_media') }}",
        dataType:"json",
        data    :{ 'id': id, '_token': '{{ csrf_token() }}' },
        success :function(res) {
          if(res.success == true){
            $('#msgSuccess').text(res.data);
            $('#media' + id).hide();
            $('#image-grid').isotope( 'reload' );
            $('#alertSuccess').show();
          }else{
            $('#msgFailed').text(res.data);
            $('#alertFailed').show();
          }
        },
        error: function(e) {
          console.log(e.responseText);
        }
      });
      $('#modalDelete').modal('hide');
    });
  });
</script>
@endsection