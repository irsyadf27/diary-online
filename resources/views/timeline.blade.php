@extends('app')

@section('title')
<title>Timeline | Diary Online</title>
@endsection

@section('sidebar')
<li class="header">Menu</li>
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li class="active"><a href="#"><i class="fa fa-clock-o"></i> Timeline</a></li>
<li><a href="{{ url('media') }}"><i class="fa fa-picture-o"></i> Media</a></li>
@endsection

@section('header')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Timeline
            <small>Diary Online</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Timeline</li>
          </ol>
        </section>
@endsection

@section('content')

          <!-- row -->
          <div class="row">
            <div class="col-md-12">
              <div id="alertFailed" style="display:none;">
                <div class="alert alert-danger alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                  <p id="msgFailed"></p>
                </div>
              </div>
              <div id="alertSuccess" style="display:none;">
                <div class="alert alert-success alert-dismissable">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                  <h4><i class="icon fa fa-check"></i> Alert!</h4>
                  <p id="msgSuccess"></p>
                </div>
              </div>
              <!-- The time line -->
              <ul class="timeline" id="tlmore">
                @foreach($data as $key => $dt)
                  <!-- timeline time label -->
                  <li class="time-label">
                    <span class="bg-red">
                      {{ $dt['tgl'] }}
                    </span>
                  </li>
                  <!-- /.timeline-label -->
                  @foreach($dt['data'] as $story)
                    <!-- timeline item -->
                    <li id="tl{{ $story['id'] }}">
                      <i class="fa fa-envelope bg-blue"></i>
                      <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> {{ $story['time'] }}</span>
                        <h3 class="timeline-header">Story: <a href="{{ url('story/read/'.$story['rand'].'/'.$story['slug']) }}">{{  $story['title']  }}</a></h3>
                        <div class="timeline-body">
                          {{ $story['body'] }}
                        </div>
                        <div class='timeline-footer'>
                          <a class="btn btn-primary btn-xs" href="{{ url('story/edit/'.$story['rand'].'/'.$story['slug']) }}">Edit</a>
                          <a class="btn btn-danger btn-xs confirm-delete" data-id="{{ $story['id'] }}" data-title="{{ $story['title'] }}" >Delete</a>
                        </div>
                      </div>
                    </li>
                    <!-- END timeline item -->
                  @endforeach
                @endforeach
                <li id="endTime"><i class="fa fa-clock-o bg-gray"></i></li>
              </ul>
            </div><!-- /.col -->
          </div><!-- /.row -->
          <div class="row text-center">
            <button class="btn btn-primary" id="loadmore" data-nextpage="2">Load more</button>
          </div>
          <div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-danger">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Want to delete?</h4>
                  </div>
                  <div class="modal-body">
                    <p id="msgDelete"></p>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Close</button>
                    <button type="button"  id="btnDelete" class="btn btn-outline">Delete</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
          </div>
@endsection

@section('js')
<script>
var nexturl = "{{ url('timeline/more?page=2') }}";
var endTime = 1;
$("#loadmore").click(function (e) { //user clicks on button
  $.ajax({
    type    :"GET",
    url     :nexturl,
    success :function(res) {
      $('#endTime').hide();
      if(res.nexturl == null){
        $('#loadmore').hide();
      }else {
        nexturl = res.nexturl;
      }
      $('#tlmore').append(res.html);
      $('#tlmore').append('<li id="endTime' + endTime + '"><i class="fa fa-clock-o bg-gray"></i></li>');
      $('#endTime' + (endTime -1)).hide();
      endTime += 1;
    },
    error: function(e) {
      console.log(e.responseText);
    }
  });
});
$('.confirm-delete').on('click', function(e) {
  e.preventDefault();

  var id = $(this).data('id');
  var title = $(this).data('title');
  $('#msgDelete').text('Are you sure you want to delete "' + title + '" ?');
  $('#modalDelete').data('title', title);
  $('#modalDelete').data('id', id).modal('show');
});
$('#btnDelete').click(function() {
  var id = $('#modalDelete').data('id');
  var title = $('#modalDelete').data('title');
  $.ajax({
    type    :"POST",
    url     :"{{ url('ajax/delete_story') }}",
    dataType:"json",
    data    :{ 'id': id, 'title': title, '_token': '{{ csrf_token() }}' },
    success :function(res) {
      if(res.success == true){
        $('#msgSuccess').text(res.data);
        $('#tl' + id).hide();
        $('#alertSuccess').show();
      }else{
        $('#msgFailed').text(res.data);
        $('#alertFailed').show();
      }
    },
    error: function(e) {
      console.log(e.responseText);
    }
  });
  $('#modalDelete').modal('hide');
});
</script>
@endsection