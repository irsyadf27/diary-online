@extends('../app')

@section('title')
<title>Account Setting | Diary Online</title>
@endsection

@section('css')
<link href="{{ asset('css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />

<style>
.prev_container{
  overflow: auto;
  height: 350px;
}
.prev_thumb{
  height: 250px;
  width: 250px;
}
</style>
@endsection

@section('sidebar')
<li class="header">Menu</li>
<li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
<li><a href="{{ url('timeline') }}"><i class="fa fa-clock-o"></i> Timeline</a></li>
<li><a href="{{ url('media') }}"><i class="fa fa-picture-o"></i> Media</a></li>
@endsection

@section('header')
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Account Setting
            <small>Diary Online</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="{{ url('/') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Account</a></li>
            <li class="active">Setting</li>
          </ol>
        </section>
@endsection

@section('content')
      <div class="row">
        <div class="col-md-12">
        @if (count($errors) > 0)
        <div class="alert alert-danger alert-dismissable">
          <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
          <h4><i class="icon fa fa-ban"></i> Alert!</h4>
          <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        </div>
        @endif
        @if (Session::get('error'))
        <div class="alert alert-danger alert-dismissable">
            <i class="fa fa-time"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Error!</b> {{ Session::get('error') }}
        </div>
        @endif
        @if (Session::get('sukses'))
        <div class="alert alert-success alert-dismissable">
            <i class="fa fa-check"></i>
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <b>Success!</b> {{ Session::get('sukses') }}
        </div>
        @endif
        </div>
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Information</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form action="{{ url('account/update') }}" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <div class="box-body">
                <div class="form-group">
                  <label>Name</label>
                  <input type="text" name="name" class="form-control" placeholder="Name" value="{{ Auth::user()->name }}">
                </div>
                <div class="form-group">
                  <label>Date of Birth</label>
                  <input type="date" id="dob" name="date_of_birth" class="form-control" placeholder="yyyy-mm-dd" value="{{ Auth::user()->tgl_lahir }}">
                </div>

                <div class="form-group">
                  <label>Textarea</label>
                  <textarea class="form-control" name="bio" rows="3" placeholder="Bio">{{ Auth::user()->bio }}</textarea>
                </div>
              </div><!-- /.box-body -->
          </div><!-- /.box -->
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Account</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                <div id="divUsername" class="form-group">
                  <label class="control-label" for="username" id="lblError">Username</label>
                  <input class="form-control" name="username" id="username" placeholder="Username" value="{{ Auth::user()->username }}" type="text">
                  <p class="help-block">min 6 - max 15</p>
                </div>
                <div class="form-group">
                  <label>Email address</label>
                  <input type="email" name="email" class="form-control" disabled="" value="{{ Auth::user()->email }}">
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" name="password" class="form-control" placeholder="Password">
                </div>
                <div class="form-group">
                  <label>Confirm Password</label>
                  <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">
                </div>
              </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>

        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Profile Pictures</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                <div class="form-group">
                  <label>Preview</label>
                  <div id="prev_file1"></div>
                </div>
                <div class="form-group">
                  <label for="exampleInputFile">File input</label>
                  <input class="file" id="file1" name="pict" type='file' multiple title="test #1"/>
                </div>
              </div><!-- /.box-body -->
          </div><!-- /.box -->
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Save Changes</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
              <div class="box-body">
                
              </div><!-- /.box-body -->
            <div class="box-footer">
              <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
          </div><!-- /.box -->
        </div>
      </div>
@endsection

@section('js')
<script src="{{ asset('/plugins/jquery.preimage.js') }}"></script>
<script src="{{ asset('/plugins/bootstrap-datepicker.js') }}"></script>
<script>
  $(document).ready(function() {
    $("#username").change(function() {
      var username = $("#username").val();

      $('#divUsername').removeClass('has-success').removeClass('has-error').addClass('has-warning');
      $("#lblError").html('<i class="fa fa-clock-o"></i> Loading');

      $.ajax({ 
          type: "POST",
          url: "{{ url('ajax/check_username') }}",
          dataType:"json",
          data: {'username': username, '_token':'{{ csrf_token() }}'},
          success: function(res) {
            if(res.fail){
              $('#divUsername').removeClass('has-success').removeClass('has-warning').addClass('has-error');
              $("#lblError").html('<i class="fa fa-times-circle-o"></i> ' + res.msg);
            } else {
              $('#divUsername').removeClass('has-error').removeClass('has-warning').addClass('has-success');
              $("#lblError").html('<i class="fa fa-check"></i> Username available');
            }
          },
          error: function(e) {
            console.log(e.responseText);
          }
      });
    });
    $('#dob').datepicker({
      format: 'yyyy-mm-dd'
    });
    $('.file').preimage();
  });
</script>
@endsection