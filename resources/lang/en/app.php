<?php

return [
	'data_success'	=> 'Successfully add data.',
	'data_failed'	=> 'Failed to add data.',
	'edit_success'	=> 'Successfully to update data.',
	'edit_failed'	=> 'Failed to update data.',
	'reg_notcomplete' => 'You must fill Account Settings',
];
