<?php
use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class UserTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		DB::table('users')->delete();
		User::create(array(
			'name'		=> 'Mohammad Irsyad Fauzan',
			'username'	=> 'irsyadf27',
			'email'		=> 'irsyad.fauzan777@gmail.com',
			'password'	=> Hash::make('123456'),
			'tgl_lahir'	=> '1997-04-27',
			'bio'		=> 'Stay Hungry, Stay Foolish!',
			'rand'		=> '123',
		));
		User::create(array(
			'name'		=> 'Administrator',
			'username'	=> 'admin',
			'email'		=> 'poticous777@gmail.com',
			'password'	=> Hash::make('123456'),
			'tgl_lahir'	=> '1997-04-27',
			'bio'		=> 'Stay Hungry, Stay Foolish!',
			'rand'		=> '270497',
		));
	}

}
