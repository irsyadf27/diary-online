<?php
namespace App;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
class Story extends Eloquent{

	protected $table = 'story';
    use SoftDeletes;
    
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
	protected $fillable = ['user_id', 'title', 'body'];
	protected $guarded = [];
	protected $hidden = [];

	function user(){
		return $this->belongsTo('App\User');
	}
    public function media(){
        return $this->belongsTo('App\Media');
    }
}
