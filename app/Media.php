<?php
namespace App;

// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;
use Jenssegers\Mongodb\Eloquent\SoftDeletes;
class Media extends Eloquent{

	protected $table = 'media';
    use SoftDeletes;
    
    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
	protected $fillable = ['user_id', 'title', 'desc', 'filename', 'mime'];
	protected $guarded = [];
	protected $hidden = [];

	function user(){
		return $this->belongsTo('App\User');
	}
}
