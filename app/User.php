<?php
namespace App;

use Illuminate\Auth\Authenticatable;
// use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Model as Eloquent;

use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

    public function story(){
        return $this->hasMany('App\Story');
    }

    public function media(){
        return $this->hasMany('App\Media');
    }
    public function test(){
    	return $this->belongsToMany('App\story', 'media', 'user_id', 'user_id')->wherePivot('a', 'a');
    }
}
