<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['middleware' => 'guest', function () {
    return View::make('index');
}]);
Route::get('home', 'HomeController@index');

/* Timeline */
Route::get('timeline', 'TimelineController@index');
Route::get('timeline/more', 'TimelineController@more');

/* Media */
Route::get('media', 'MediaController@index');
Route::get('media/more', 'MediaController@more');
Route::get('media/thumb_{md5}', 'MediaController@getthumb');
Route::get('media/{md5}', 'MediaController@getfile');

/* Story */
Route::get('story', 'StoryController@index');
Route::get('story/trash', 'StoryController@trash');
Route::get('story/draft', 'StoryController@draft');
Route::get('story/edit/{rand}/{slug}', 'StoryController@edit');
Route::get('story/read/{rand}/{slug}', 'StoryController@read');
Route::get('story/addstory', 'StoryController@addstory');
Route::post('story/new', 'StoryController@create');
Route::post('story/edit', 'StoryController@update');

/* Ajax */
Route::post('ajax/save_draft', 'AjaxController@save_draft');
Route::post('ajax/delete_story', 'AjaxController@delete_story');
Route::post('ajax/forcedelete_story', 'AjaxController@force_delete_story');
Route::post('ajax/restore_story', 'AjaxController@restore_story');

Route::post('ajax/addmedia', 'AjaxController@addmedia');
Route::post('ajax/delete_media', 'AjaxController@delete_media');

Route::post('ajax/check_username', 'AjaxController@check_username');

/* Account */
Route::get('account/setting', 'AccountController@setting');
Route::post('account/update', 'AccountController@update');
Route::get('account/img/{width}x{height}/{user_id}', 'AccountController@getthumb');
Route::get('account/img/{user_id}', 'AccountController@getimg');
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
