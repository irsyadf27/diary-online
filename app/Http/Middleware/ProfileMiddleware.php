<?php namespace App\Http\Middleware;

use Closure;
use Auth;
use Lang;
class ProfileMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
        if (Auth::user()->editprofile < 1)
        {
            return redirect('account/setting')
            	->with('error', Lang::get('app.reg_notcomplete'));
        }

        return $next($request);
	}

}
