<?php
namespace App\Http\Controllers;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Jenssegers\Date\Date;
use App\Media;
use App\Story;
use App\Aes;
use Auth;
use Crypt;
use Hashids;
use Request;
class MediaController extends Controller {

	public function __construct(){
		$this->middleware('auth');
		$this->middleware('profile');
	}

	public function index(){
		$media = Media::orderBy('created_at', 'desc')
				->where('user_id', Auth::user()->id)
				->get();
		return view('media.media', array('data' => $media));
	}
 
	public function getfile($md5){
	
		$entry = Media::where('md5_time', '=', $md5)->firstOrFail();
		$dir = 'uploads/userdata/'.Auth::user()->id.'/';
		$file = Storage::disk('local')->get($dir.$entry->filename);
		$img = Image::make($file);
		//return (new Response($file, 200))->header('Content-Type', $entry->mime);
		return (new Response($img->encode('png'), 200))->header('Content-Type', 'image/png');
	}

	public function getthumb($md5){
		$entry = Media::where('md5_time', '=', $md5)->firstOrFail();
		$dir = 'uploads/userdata/'.Auth::user()->id.'/';
		$file = Storage::disk('local')->get($dir.$entry->filename);
		$img = Image::make($file);
		$img->resize(300, null, function ($constraint) {
			$constraint->aspectRatio();
		});
		//return (new Response($img, 200))->header('Content-Type', $entry->mime);
		return (new Response($img->encode('png'), 200))->header('Content-Type', 'image/png');
	}

	public function more(){
		if(!Request::ajax()){
			//die('Fvk you !!!');
		}
		$media = Media::orderBy('created_at', 'desc')
				->where('user_id', Auth::user()->id)
				->paginate(2);
		$html = '';

		foreach($media as $data){
			$html .= '<div class="col-md-3 image-item"><div class="box">';
			if(!empty($data->title)){
				$html .= '<div class="box-header"><p class="box-title">'.$data->title.'</p></div>';
			}
			$html .= '<div class="box-body"><img class="img-responsive" style="width:100%;" src="' . url('media/thumb_'.$data->md5_time) .'"/></div><!-- /.box-body -->';
            $html .= '<div class="box-footer"><p>'.$data->desc.'</p><h6 class="text-muted"><i class="fa fa-clock-o"></i> '.$data->created_at.'</h6></div>';
            $html .= '</div><!-- /.box --></div>';
		}
		return array('nexturl' => $media->nextPageUrl(), 'html' => $html);
	}
}