<?php
namespace App\Http\Controllers;
use Jenssegers\Date\Date;
use Illuminate\Http\Response;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Auth;
use App\User;
use Input;
use Hash;
use Lang;
class AccountController extends Controller {

	public function __construct(){
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function setting(){
		return view('account.setting');
	}

	public function getimg($user_id){
		$entry = User::find($user_id);
		$dir = 'uploads/userdata/'.$user_id.'/profile/';
		$file = Storage::disk('local')->get($dir.$entry->foto);
		$img = Image::make($file);
		//return (new Response($file, 200))->header('Content-Type', $entry->mime);
		return (new Response($img->encode('png'), 200))->header('Content-Type', 'image/png');
	}

	public function getthumb($width, $height, $user_id){
		$entry = User::find($user_id);
		$dir = 'uploads/userdata/'.$user_id.'/profile/';
		$file = Storage::disk('local')->get($dir.$entry->foto);
		$img = Image::make($file);
		$img->resize($width, $height);
		//return (new Response($img, 200))->header('Content-Type', $entry->mime);
		return (new Response($img->encode('png'), 200))->header('Content-Type', 'image/png');
	}

	public function update(){
		$rules = array(
			'name'	=> 'between:3,32',
			'date_of_birth'	=> 'required|date_format:Y-m-d',
			'username' => 'required|alpha_num|between:6,15',
			'password' => 'between:6,32|confirmed',
			'password_confirmation' => 'between:6,32|min:3',
			'pict'	=> 'mimes:jpg,jpeg,png|max:5000'
	    );
		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return Redirect::back()
				->withErrors($validator)
				->withInput(); 
		} else {
			if(User::where('username', '=', Input::get('username'))
				->where('_id', '<>', Auth::user()->id)
				->exists()){
				return Redirect::back()
					->with('error', 'Username has been taken')
					->withInput(); 
			}
			$update = User::find(Auth::user()->id);
			$update->name = Input::get('name');
			$update->tgl_lahir = Input::get('date_of_birth');
			$update->bio = Input::get('bio');
			$update->username = Input::get('username');
			if($update->editprofile < 1){
				$update->rand = rand(0,10000);
			}
			if(Input::get('password')){
				$update->password = Hash::make(Input::get('password'));
			}
			$file = Input::file('pict');
			if(Input::hasFile('pict')){
				$dest_dir = 'uploads/userdata/'.Auth::user()->id.'/profile/';
				$extension = $file->getClientOriginalExtension();
				Storage::disk('local')->put($dest_dir.Auth::user()->id.'.'.$extension,  File::get($file));
				$update->foto = Auth::user()->id.'.'.$extension;
			}
			$update->increment('editprofile');
			if($update->save()) {
				return Redirect::to('account/setting')->with('sukses', Lang::get('app.data_success'));;
			} 
		}
	}

}
