<?php
namespace App\Http\Controllers;
use Jenssegers\Date\Date;
use App\Story;
use App\Aes;
use Auth;
use Crypt;
use Hashids;
use Request;

class TimelineController extends Controller {

	public function __construct(){
		$this->middleware('auth');
		$this->middleware('profile');
	}

	public function index(){
		$post = Story::orderBy('created_at', 'desc')
				//->groupBy('created_at')
				->where('user_id', Auth::user()->id)
				->where('save', 'story')
				->select('id', 'slug', 'title', 'body', 'created_at', 'rand')
				->paginate(10);
		$data = [];
		$key = Hashids::encode(Auth::user()->rand);
		$aes = new Aes;
		foreach($post as $asu){
			$date = new Date($asu->created_at);
			$data[$date->format('dMY')]['tgl'] = $date->format('D, d M Y');
			$data[$date->format('dMY')]['data'][] = array(
				'id'			=> $asu->id,
				'created_at'	=> $date->format('D, d M Y'),
				'time'			=> $date->format('H:i'),
				'slug'			=> $asu->slug,
				'rand'			=> $asu->rand,
				'title'			=> str_limit($asu->title, 30),
				'body'			=> str_limit(strip_tags($aes->decode($asu->body, $key)), 255)
			);
		}

		return view('timeline', array('data' => $data));
		//print_r($post);
	}

	public function more(){
		if(!Request::ajax()){
			die('Fvk you !!!');
		}
		$post = Story::orderBy('created_at', 'desc')
				//->groupBy('created_at')
				->where('user_id', Auth::user()->id)
				->where('save', 'story')
				->select('id', 'slug', 'title', 'body', 'created_at', 'rand')
				->paginate(10);
		$data = [];
		$key = Hashids::encode(Auth::user()->rand);
		$aes = new Aes;
		foreach($post as $asu){
			$date = new Date($asu->created_at);
			$data[$date->format('dMY')]['tgl'] = $date->format('D, d M Y');
			$data[$date->format('dMY')]['data'][] = array(
				'id'			=> $asu->id,
				'created_at'	=> $date->format('D, d M Y'),
				'time'			=> $date->format('H:i'),
				'slug'			=> $asu->slug,
				'rand'			=> $asu->rand,
				'title'			=> str_limit($asu->title, 30),
				'body'			=> str_limit(strip_tags($aes->decode($asu->body, $key)), 255)
			);
		}
		$html = '';
		foreach($data as $dt){
			$html .= '<li class="time-label"><span class="bg-red">'.$dt['tgl'].'</span></li>';
			foreach($dt['data'] as $story){
				$html .= '<li id="tl'. $story['id'] .'">
                      <i class="fa fa-envelope bg-blue"></i>
                      <div class="timeline-item">
                        <span class="time"><i class="fa fa-clock-o"></i> '. $story['time'] .'</span>
                        <h3 class="timeline-header">Story: <a href="'. url('story/read/'.$story['rand'].'/'.$story['slug']) .'">'.  $story['title']  .'</a></h3>
                        <div class="timeline-body">
                          '. $story['body'] .'
                        </div>
                        <div class="timeline-footer">
                          <a class="btn btn-primary btn-xs" href="'. url('story/edit/'.$story['rand'].'/'.$story['slug']) .'">Edit</a>
                          <a class="btn btn-danger btn-xs confirm-delete" data-id="'. $story['id'] .'" data-title="'. $story['title'] .'" >Delete</a>
                        </div>
                      </div>
                    </li>';
			}
		}
		return array('nexturl' => $post->nextPageUrl(), 'html' => $html);
	}
}