<?php
namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Jenssegers\Date\Date;
use Auth;
use Lang;
use Input;
use Hashids;
use Crypt;
use Request;
use Response;
use App\Story;
use App\User;
use App\Aes;
use App\Media;
use App\Fileentry;
class AjaxController extends Controller {

	public function __construct(){
		$this->middleware('auth');
		$this->middleware('profile', ['except' => ['check_username']]);
	}

	public function check_username(){
		if (!Request::ajax()){
			die("Fvck you !!!");
		}
		$rules = array(
			'username' => 'required|alpha_num|between:6,15'
			);
		$validator = Validator::make(Input::all(), $rules);
		if($validator->fails()){
			return Response::json(array(
				'fail'	=> true,
				'msg'	=> 'Error: '.$validator->messages()->first()
				));
		} else {
			if(User::where('username', '=', Input::get('username'))->exists()){
				return Response::json(array(
					'fail'	=> true,
					'msg'	=> 'Username has been taken.'
					));
			} else {
				return Response::json(array(
					'fail'	=> false,
					'msg'	=> 'iloveyou'
					));
			}
		}
	}

	/* Story */
	public function save_draft(){
		if (Request::ajax()){
			//check database
			$title = Input::get('title') ? Input::get('title') : 'Story-'.date('Y-m-d H:i:s');
			$body = Input::get('story');
			$s_story = Input::get('save_story');
			$s_draft = Input::get('save_draft');
			$rand = Input::get('rand');
			$key = Hashids::encode(Auth::user()->rand);
			$aes = new Aes;
			$check = Story::where('user_id', Auth::user()->id)
					->where('rand', $rand)
					->count();
			if($check < 1){
				$save = new Story;
				$save->user_id = Auth::user()->id;
				$save->title = $title;
				$save->body = $aes->encode($body, $key);
				$save->slug = str_slug($title, '-');
				$save->rand = $rand;
				$save->save = 'draft';
				$save->save();
			} else {
				$update = Story::where('rand', $rand)->first();
				$update->user_id = Auth::user()->id;
				$update->title = $title;
				$update->body = $aes->encode($body, $key);
				$update->slug = str_slug($title, '-');
				$update->save();
			}
		} else {
			return "Fvck you !!!";
		}
	}

	public function delete_story(){
		if (Request::ajax()){
			$id = Input::get('id');
			$title = Input::get('title');
			$find = Story::find($id);
			if($find->delete()){
				return Response::json(['success'=>true, 'data'=>'Successfully delete "'.$title.'"']);
			} else {
				return Response::json(['success'=>false, 'data'=>'Failed to delete "'.$title.'"']);
			}
		} else {
			return "Fvck you !!!";
		}
	}

	public function force_delete_story(){
		if (Request::ajax()){
			$id = Input::get('id');
			$title = Input::get('title');
			$find = Story::find($id);
			if($find->forceDelete()){
				return Response::json(['success'=>true, 'data'=>'Successfully to force delete "'.$title.'"']);
			} else {
				return Response::json(['success'=>false, 'data'=>'Failed to force delete "'.$title.'"']);
			}
		} else {
			return "Fvck you !!!";
		}
	}

	public function restore_story(){
		if (Request::ajax()){
			$id = Input::get('id');
			$title = Input::get('title');
			$find = Story::withTrashed()->find($id);
			if($find->restore()){
				return Response::json(['success'=>true, 'data'=>'Successfully restore "'.$title.'"']);
			} else {
				return Response::json(['success'=>false, 'data'=>'Failed to restore "'.$title.'"']);
			}
		} else {
			return "Fvck you !!!";
		}
	}
	/* End of story */

	/* Begin of media */
	public function addmedia(){
		if (Request::ajax()){
			$rules = array(
				'title'	=> '',
				'desc'	=> '',
				'pict'	=> 'required|mimes:jpg,jpeg,png|max:5000',
		    );
			$validator = Validator::make(Input::all(), $rules);
			if($validator->fails()){
				return Response::json(array(
					'fail'	=> true,
					'msg'	=> 'Error: Validator'
					));
			} else {


				$file = Input::file('pict');
				$md5_time = md5(time());
				$title = Input::get('title');
				$desc = Input::get('desc');

				$dest_dir = 'uploads/userdata/'.Auth::user()->id.'/';
				$extension = $file->getClientOriginalExtension();
				Storage::disk('local')->put($dest_dir.$file->getFilename().'.'.$extension,  File::get($file));
				$entry = new Media();
				$entry->user_id = Auth::user()->id;
				$entry->title = $title;
				$entry->desc = $desc;
				$entry->mime = $file->getClientMimeType();
				$entry->original_filename = $dest_dir.$file->getClientOriginalName();
				$entry->filename = $file->getFilename().'.'.$extension;
				$entry->md5_time = $md5_time;

				if($entry->save()) {  
					return Response::json(array(
						'success'	=> true,
						'msg'		=> 'Upload succss.',
						'url'		=> url('media/thumb_'.$md5_time),
						'title'		=> $title,
						'desc'		=> $desc,
						'created_at'=> date('Y-m-d H:i:s')
						));
				} else {
					return Response::json(array(
						'fail'	=> true,
						'msg'	=> 'Failed to upload.'
						));
				}
			}
		} else {
			return "Fvck you !!!";
		}
	}
	public function delete_media(){
		if (Request::ajax()){
			$id = Input::get('id');
			$find = Media::find($id);
			if($find->delete()){
				return Response::json(['success'=>true, 'data'=>'Successfully delete data.']);
			} else {
				return Response::json(['success'=>false, 'data'=>'Failed to delete data.']);
			}
		} else {
			return "Fvck you !!!";
		}
	}
}
