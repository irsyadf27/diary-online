<?php
namespace App\Http\Controllers;
use Jenssegers\Date\Date;
use Auth;
use App\User;
use App\Story;
class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct(){
		$this->middleware('auth');
		$this->middleware('profile');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index(){
		$date = new Date(Auth::user()->tgl_lahir);
		$data = array(
			'jml_story' => Story::where('user_id', Auth::user()->id)->where('save', 'story')->count(),
			'jml_draft' => Story::where('user_id', Auth::user()->id)->where('save', 'draft')->count(),
			'jml_trash' => Story::onlyTrashed()->where('user_id', Auth::user()->id)->count(),
			'tgl_lahir1' => $date->format('D, d-m-Y'),
			'tgl_lahir2' => $date->ago()
			);
		return view('home', $data);
	}


}
