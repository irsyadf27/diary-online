<?php
namespace App\Http\Controllers;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

use Jenssegers\Date\Date;
use Auth;
use Lang;
use Input;
use Hashids;
use Crypt;
use App\Story;
use App\Aes;
class StoryController extends Controller {

	public function __construct(){
		$this->middleware('auth');
		$this->middleware('profile');
	}

	public function index(){
		$post = Story::orderBy('created_at', 'desc')
				->where('user_id', Auth::user()->id)
				->where('save', 'story')
				->get();
		return view('story.story', array('data' => $post));
	}

	public function draft(){
		$post = Story::orderBy('created_at', 'desc')
				->where('user_id', Auth::user()->id)
				->where('save', 'draft')
				->get();
		return view('story.draft', array('data' => $post));
	}

	public function trash(){
		$post = Story::orderBy('created_at', 'desc')
				->onlyTrashed()
				->where('user_id', Auth::user()->id)
				->get();
		return view('story.trash', array('data' => $post));
	}

	/* Form add story */
	public function addstory(){
		// return dd(Story::groupBy('created_at')->get());
		return view('story.add');
	}

	/* Add new story */
	public function create(){
		$rules = array(
			'title'		=> 'required',
			'story'		=> 'required|min:20'
	    );
		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return Redirect::to('story/addstory')
				->withErrors($validator)
				->withInput(); 
		} else {
			$title = Input::get('title') ? Input::get('title') : 'Story-'.date('Y-m-d H:i:s');
			$body = Input::get('story');
			$s_story = Input::get('save_story');
			$s_draft = Input::get('save_draft');
			$rand = Input::get('rand');
			$key = Hashids::encode(Auth::user()->rand);
			$aes = new Aes;
			$check = Story::where('user_id', Auth::user()->id)
							->where('rand', $rand)
							->count();
			if($check > 0){
				$update = Story::where('rand', $rand)->first();
				$update->user_id = Auth::user()->id;
				$update->title = $title;
				$update->body = $aes->encode($body, $key);
				$update->slug = str_slug($title, '-');
				$update->save = ($s_draft) ? 'draft' : 'story';
				$update->save();
			}else{
				$save = new Story;
				$save->user_id = Auth::user()->id;
				$save->title = $title;
				$save->body = $aes->encode($body, $key);
				$save->slug = str_slug($title, '-');
				$save->rand = $rand;
				$save->save = ($s_draft) ? 'draft' : 'story';
				$save->save();
			}
			return Redirect::to('story/addstory')->with('sukses', Lang::get('app.data_success'));;
		}
	}

	public function read($rand, $slug){
		$post = Story::where('user_id', Auth::user()->id)
				->where('rand', $rand)
				->where('slug', $slug)
				->first();
		$data = [];
		$key = Hashids::encode(Auth::user()->rand);
		$aes = new Aes;

		$date = new Date($post->created_at);
		$data = array(
			'created_at'	=> $date->format('D, d M Y'),
			'time'			=> $date->format('H:i'),
			'slug'			=> $post->slug,
			'title'			=> str_limit($post->title, 30),
			'title_full'	=> $post->title, 30,
			'body'			=> str_limit(strip_tags($aes->decode($post->body, $key)), 255),
			'body_full'		=> $aes->decode($post->body, $key)
		);
		return view('story.read', array('data' => $data));
	}

	public function edit($rand, $slug){
		$post = Story::where('user_id', Auth::user()->id)
				->where('rand', $rand)
				->where('slug', $slug)
				->first();
		$data = [];
		$key = Hashids::encode(Auth::user()->rand);
		$aes = new Aes;

		$post->body = $aes->decode($post->body, $key);
		return view('story.edit', $post);
	}

	/* Save edit story */
	public function update(){
		$rules = array(
			'title'		=> 'required',
			'save_as'		=> 'required',
			'story'		=> 'required|min:20'
	    );
		$validator = Validator::make(Input::all(), $rules);

		if($validator->fails()){
			return Redirect::back()
				->withErrors($validator)
				->withInput(); 
		} else {
			$title = Input::get('title') ? Input::get('title') : 'Story-'.date('Y-m-d H:i:s');
			$body = Input::get('story');
			$save_as = Input::get('save_as');
			$rand = Input::get('rand');
			$key = Hashids::encode(Auth::user()->rand);
			$aes = new Aes;

			$update = Story::where('rand', $rand)->first();
			$update->title = $title;
			$update->body = $aes->encode($body, $key);
			$update->slug = str_slug($title, '-');
			$update->save = ($save_as == 'draft') ? 'draft' : 'story';
			$update->save();
			
			return Redirect::to('story/edit/'.$rand.'/'.str_slug($title, '-'))->with('sukses', Lang::get('app.edit_success'));;
		}
	}
}